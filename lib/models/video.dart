class Video {
  final String id;
  final String title;
  final String thumbnailURL;
  final String channelTitle;

  Video({
    this.id,
    this.title,
    this.thumbnailURL,
    this.channelTitle,
  });

  factory Video.fromMap(Map<String, dynamic> map) {
    return Video(
      id: map['resourceId']['videoId'],
      title: map['title'],
      thumbnailURL: map['thumbnails']['high']['url'],
      channelTitle: map['channelTitle'],
    );
  }
}
