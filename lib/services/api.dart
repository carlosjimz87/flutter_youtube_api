import 'dart:convert';
import 'dart:io';
import 'package:YoutubeApi/models/channel.dart';
import 'package:YoutubeApi/models/video.dart';
import 'package:YoutubeApi/utils/keys.dart';
import 'package:http/http.dart' as http;

class ApiService {
  final String _baseUrl = 'www.googleapis.com';
  String _nextPageToken = '';

  ApiService._instantiate();
  static final ApiService instance = ApiService._instantiate();

  Future<Channel> fetchChannel({String channelId}) async {
    Map<String, String> parameters = {
      'part': 'snippet,contentDetails,statistics',
      'id': channelId,
      'key': API_KEY,
    };

    Uri uri = Uri.https(
      _baseUrl,
      '/youtube/v3/channels',
      parameters,
    );

    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };

    // Get request
    var response = await http.get(uri, headers: headers);

    // handle response
    Map<String, dynamic> data = null;
    if (response.statusCode == 200) {
      try {
        data = json.decode(response.body)['items'][0];
      } catch (e) {
        throw json.decode(response.body)['error']['message'];
      }

      Channel channel = Channel.fromMap(data);

      // fetch first batch of videos

      channel.videos = await fectchVideosFromPlaylist(
        playlistId: channel.uploadPlaylistId,
      );
      return channel;
    } else {
      throw json.decode(response.body)['error']['message'];
    }
  }

  Future<List<Video>> fectchVideosFromPlaylist({String playlistId}) async {
    Map<String, String> parameters = {
      'part': 'snippet',
      'playlistId': playlistId,
      'maxResults': '8',
      'pageToken': _nextPageToken,
      'key': API_KEY
    };

    Uri uri = Uri.https(
      _baseUrl,
      '/youtube/v3/playlistItems',
      parameters,
    );

    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };

    // Get playlist request
    var response = await http.get(uri, headers: headers);

    //handle response
    if (response.statusCode == 200) {
      var data = json.decode(response.body);

      _nextPageToken = data['nextPageToken'] ?? '';
      List<dynamic> videosJson = data['items'];

      // fetch first 8 videos of the playlist
      List<Video> videos = [];

      videosJson.forEach(
        (json) => videos.add(
          Video.fromMap(json['snippet']),
        ),
      );
      return videos;
    } else {
      throw json.decode(response.body)['error']['message'];
    }
  }
}
